# python3 -m reveng.app
import os 
import subprocess

class App(object):
    ''' App class '''
    extension = '.apk'
    separator = '_'
    #Public
    def __init__(self, path, family = 'noFamily', givenName ='noGivenName'):
        ''' init app with the path, family name and family processing name'''
        self.__location = ''
        self.__processingName = ''
        self.__permissionStrings = []
        self.__permissionPhrases = []
        self.__package = ''
        self.__featuresSelected = []
        self.__featureExtracted = []
        self.__family = ''
        try:
            if self.__checkPathsExistance(path):
                self.__location = path
                self.__name = os.path.basename(self.__location)
                self.__family = family
                self.__processingName = givenName
        except:
            print('Terminated due to some exceptions in App')
    
    def parse(self):
        ''' exec aapt on the app '''
        self.__exec()

    def extractFeature(self, featureSelected):
        ''' extract selected features from app list of features '''
        if not featureSelected == []:
            self.__featuresSelected = featureSelected
            self.__featureExtracted = [None] * len(self.__featuresSelected)

            for f in self.__featuresSelected:
                if f in self.__permissionPhrases:
                    self.__featureExtracted[self.__featuresSelected.index(f)] = 1
        else:
            raise ValueError('No feature was selected')

    #getters
    def getFeatureExtracted(self):
        if not self.__featuresSelected == []:
            if not self.__featureExtracted == []:
                return self.__featureExtracted
            else:
                raise ValueError('No feature was extracted')
        else:
            raise ValueError('No feature was selected')

    def getName(self): 
        return self.__name

    def getProcessingName(self):
        return self.__processingName

    def getPermissionStrings(self): 
        return self.__permissionStrings

    def getPermissionPhrases(self): 
        return self.__permissionPhrases

    def getPackage(self):
        return self.__package

    def getFamily(self):
        return self.__family

    #Private   
    def __checkPathsExistance(self, loc):
        ''' check existnace family path'''
        if os.path.exists(loc):
            #print('App file: found')
            return True
        else:
            raise ValueError('App file: not found at location: ', loc)
            return False 

    def __exec(self):
        ''' exec aapt and extract permissions strings and phrases'''
        try:
            outlist = (subprocess.check_output(['aapt', 'd', 'permissions', self.__location], shell=False)).splitlines()
        except subprocess.CalledProcessError as aaptexc:                                                                                                   
            print ('error code ' + aaptexc.returncode + ' ' + aaptexc.output)
        
        # decode byte to str        
        outlist = [o.decode() for o in outlist]

        # extract package name
        for pck in outlist:
            if pck.startswith('package: ', 0):
                self.__package = pck[9:]
        
        # extract the permission phrase from permission string
        self.__permissionStrings = list(set([p[32:-1] for p in outlist if p.startswith("uses-permission: name='android.permission.", 0)]))
        self.__permissionPhrases = list(set([p[42:-1] for p in outlist if p.startswith("uses-permission: name='android.permission.", 0)]))

        # sort the list 
        self.__permissionStrings.sort()
        self.__permissionPhrases.sort()
