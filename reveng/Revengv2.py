import os
import shutil
import json
import subprocess
import re
import threading
import Queue
import time 

class Reveng:
    ''' Reverse Engineering class '''

    #def __init__(self, datasetOnLocation, collectionName = "FeatureExtraction" , featureOnLocation):
    def __init__(self):
        ''' get the dataset location '''
        datasetOnLocation = "/home/fahad/Desktop/Testing/dataset"
        featureOnLocation = "/home/fahad/Desktop/Testing/StormDroid.txt"
        collectionName = "collection"
        if os.path.exists(datasetOnLocation) and os.path.exists(featureOnLocation):
            self.datasetPath = datasetOnLocation
            self.featurePath = featureOnLocation
            self.newDatasetFoldername = collectionName
            self.extension = ".apk"
            self.jsonFilename = "index.json"
            self.separator = "_"

        else:
            raise IOError("On of the following locations doesn't exist:\n  Dataset: " \
            + datasetOnLocation +", OR\nFeature: " + featureOnLocation)

    def prepare(self):
        ''' build json for the dataset, move and rename the files. Also load the features '''
        self.jdata = {}
        self.jdata["dataset"] = {}
        self.jdata["dataset"]["location"] = self.datasetPath 
        self.newDatasetPath = os.path.join(os.path.dirname(self.datasetPath), self.newDatasetFoldername)
        if not os.path.exists(self.newDatasetPath):
            os.mkdir(self.newDatasetPath)
        self.jdata["dataset"]["processingLocation"] = self.newDatasetPath 
        self.families = []
        self.family = []
        self.familiesTemp = []

        # list of malware classes
        self.y = []
        #self.yMember = 0
        for self.root, self.folders, self.files in os.walk(self.datasetPath):
            
            if self.root == self.datasetPath:
                self.jdata["dataset"]["size"] = len(self.folders)
                self.familiesTemp = self.folders
                
                
            if os.path.basename(self.root) in self.familiesTemp:
                self.files = [f for f in self.files if f.endswith(self.extension)]
                self.family = {}
                self.apps = []
                self.appNo = 0
                self.family["name"] = os.path.basename(self.root)
                self.family["sequence"] = self.familiesTemp.index(os.path.basename(self.root))
                
                self.family["size"] = len(self.files)
                
                for self.fname in self.files:
                    self.member = {}
                    #self.yMember = [0] * len(self.familiesTemp)
                    try:
                        self.member["name"] = self.fname 
                        # check for dest path existance
                        self.destPath = os.path.join(os.path.dirname(self.datasetPath),self.newDatasetFoldername)
                        if os.path.exists(self.destPath):
                            self.newName = str(self.appNo) + self.separator + str(self.familiesTemp.index(os.path.basename(self.root))) + self.extension

                            self.apkLocation = os.path.join(self.root, self.fname)
                            shutil.copyfile(self.apkLocation, os.path.join(self.destPath, self.newName))

                            self.member["processingName"] = self.newName
                        else:
                            self.member["processingName"] = "Error"
                        self.member["feature"] = None
                        self.yMember = self.family["sequence"] 
                        self.y.append(self.yMember)
                        
                        self.apps.append(self.member)
                        self.appNo += 1
                    except Exception , e:
                        print "user Exception msg: " + str(e)

                self.family["apps"] = self.apps
                self.families.append(self.family)
        self.jdata["dataset"]["families"] = self.families
        
        # Fetch features file text 
        with open(self.featurePath) as d:
            self.featureSelected = d.readlines()
        
        self.featureSelected = [f[:-1] for f in self.featureSelected] # -1 for \n at the end of a permission phrase
        #self.apkFeature = [0] * (len(self.featureSelected)+1) # initialize the list with zero [0,..,0] + 1 for the family seq

    def __insertFeature(self, apkPath, ft, pkg):
        ''' update json data by inserting the apkFeature ft and package pkg in apkPath'''
        # extract familyName from apkPath 
        self.apkname = os.path.basename(apkPath) 
        self.fSeq = int(self.apkname[(self.apkname.find("_",0,len(self.apkname)-4))+1:len(self.apkname)-4])
        
        for f in self.jdata["dataset"]["families"]:
            if f["sequence"] == self.fSeq:
                for m in f["apps"]:
                    if m["processingName"] == self.apkname:
                        m["feature"] = ft
                        m["package"] = pkg
                        break
                break

    def __run(self, apkPath):
        ''' task for feature extraction '''

        try:
            self.outlist = (subprocess.check_output(['aapt', 'd', 'permissions', apkPath], shell=False)).splitlines()
        except subprocess.CalledProcessError as aaptexc:                                                                                                   
            print "error code", aaptexc.returncode, aaptexc.output
        
        
        # extract package name
        self.package = ""
        for pck in self.outlist:
            if pck.startswith("package: ", 0):
                self.package = pck[9:]

        # extract the permission phrase from permission string
        self.permPhrase= set([p[42:-1] for p in self.outlist if p.startswith("uses-permission: name='android.permission.", 0)])

        # self.feature => self.featureSelected
        # self.apkFeature => self.featureExtracted
        self.featureExtracted = [0] * len(self.featureSelected)
        for dx in range(len(self.featureSelected)):
            if self.featureSelected[dx] in self.permPhrase:
                self.featureExtracted[dx] = 1
            else:
                self.featureExtracted[dx] = 0
        # append family class to the featureExtracted
        #self.featureExtracted.append(int(os.path.basename(apkPath)[os.path.basename(apkPath).find("_", 0)+1:-4]))
        
        #self.yMember = [0] * len(self.familiesTemp)
        #self.yMember[int(os.path.basename(apkPath)[os.path.basename(apkPath).find("_", 0)+1:-4])] = 1
        #self.y.append(self.yMember)
        obj = (apkPath,self.featureExtracted, self.package)
        self.reqs.put(obj)

    def runFeatureExtraction(self):
        ''' feature extraction from apk and update json '''
        self.noOfThreads = 4

        self.q = Queue.Queue() # tasks for exec by threads
        self.reqs = Queue.Queue() # tasks of update reqs for the upThread

        # l = apks paths
        l = [ os.path.join(self.newDatasetPath, i) for i in os.listdir(self.newDatasetPath) if i.endswith(self.extension)]
        self.read = len(l)
        self.write = 0
        for item in l:
            self.q.put(item)
        print "in queue:" + str(self.q.qsize())
        print "Starting...\n"
        
        # exec threads
        for i in range(self.noOfThreads):
            t = threading.Thread(target=self.__worker)
            t.daemon = True
            t.start()

        # update thread
        upThread = threading.Thread(target=self.__workerUpdate)
        upThread.daemon = True
        upThread.start()

        self.q.join()    # block until all tasks are done
        self.reqs.join() # block until all update tasks are done

        # dump the jsonData dict in dataset.json file
        with open(self.jsonFilename, "w") as self.outfile:
            json.dump(self.jdata, self.outfile)

        print "read:" + str(self.read) + " files"
        print "written:" + str(self.write) + " files"
        print "Exeting...\n"

    def __worker(self):
        ''' thread worker '''
        while True:
            item = self.q.get()
            self.__run(item) 
            self.q.task_done()
    
    def __workerUpdate(self):
        ''' json updater thread '''
        
        while True:
            item = self.reqs.get()
            self.__insertFeature(item[0],item[1], item[2])
            self.write = self.write + 1
            self.reqs.task_done()
#
# execution
#

start = time.time()

d = Reveng()
p = d.prepare()
r = d.runFeatureExtraction()

end = time.time()
print "..Total ="  + str(end - start)

f = open("classes.txt", 'w')
for i in d.y:
    f.write(str(i))
    f.write("\n")
f.close()

x = []



for i in d.jdata["dataset"]["families"]:
    for j in i["apps"]:
        x.append(j["feature"])


f = open("features.txt", 'w')
for i in x:
    f.write(str(i))
    f.write("\n")
f.close()


import numpy as np
from sklearn.cross_validation import StratifiedShuffleSplit
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.neural_network import MLPClassifier




X = np.array(x)
y = np.array(d.y)
#print x
#print y 

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.8, random_state=42)

#print X.shape
#print y.shape


clf = SVC()
clf.fit(X_train, y_train)


result=clf.score(X_test, y_test, sample_weight=None) 
print('svc:'+str(result))

#http://scikit-learn.org/stable/modules/neural_networks_supervised.html
mlp = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(5, 2), random_state=1)
mlp.fit(X_train, y_train)
result=mlp.score(X_test, y_test, sample_weight=None) 
print('mlp:'+str(result))