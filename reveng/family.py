# python3 -m reveng.family
import os 
from . import app

class Family(object):
    ''' family class '''
    #Public
    def __init__(self, path, givenName = 'noGivenName'):
        ''' init family obj '''
        self.__name = ''
        self.__processingName = ''  
        self.__apps = []
        self.__permissionsUnion = []
        self.__permissionsIntersection = []
        self.__permissionsXOR = []
        self.__location = ''
        self.__featuresSelected = []
        try:
            if self.__checkPathsExistance(path):
                self.__location = path
                self.__name = os.path.basename(self.__location)
                self.__processingName = givenName
        except:
            print('Terminated due to some exceptions in Family')

    def parse(self): 
        ''' parse the family: call App.parse() '''
        if not self.__apps == []:
            for p in self.__apps:
                p.parse()
        else:
            raise ValueError('No apps were loaded')
    
    def extractFeature(self, featureSelected = []):
        ''' extract selected features from all apps '''
        if not featureSelected == [] or not self.__featuresSelected == []:
            if not featureSelected == []:
                self.__featuresSelected = featureSelected
            if not self.__apps == []:
                for p in self.__apps:
                    p.extractFeature(self.__featuresSelected)
            else:
                raise ValueError('No apps were loaded')
        else:
            raise ValueError('No feature was selected')

    def loadFamilyContent(self):
        ''' create app objs, insert theSm in apps '''
        fils = [fil for fil in os.listdir(self.__location) if fil.endswith('.apk') and not fil.startswith('.')]
        self.__apps = [None] * len(fils)
        ndx = 0
        for ap in fils:
            # <FamilyProcName> <App.seperator> <AppProcName> <App.extension>
            #appProcName = ''.join([self.__processingName, App.separator, str(ndx), App.extension])
            appObj = app.App(os.path.join(self.__location, ap), self.__name, str(ndx) ) # create an app
            self.__apps[ndx] = appObj
            ndx = ndx + 1

        if self.__apps == []:
            raise ValueError('No apps in:', self.__name, 'folder')

    #getters
    def getAppObj(self, name = '', procName = ''):
        ''' return an app object with name or procName'''
        
        if self.__appsEmpty(): 
            return None

        result = [ap for ap in self.__apps if ap.getName() == name or ap.getProcessingName() == procName]
        if len(result) > 1:
            print("conflict: More than one app with name:", name, "or processingName:", procName)
            return None
        elif len(result) < 1:
            print("No app with name:", name, "and processingName:", procName)            
            return None
        else:
            return result[0]

    def getAllAppsObjs(self):
        ''' return all Apps objects in the family '''

        if self.__appsEmpty(): 
            return None
        else:           
            return self.__apps

    def getAllAppsNames(self):
        ''' return a list of all apps name '''
        if self.__appsEmpty(): 
            return None
        else:
            return [p.getName() for p in self.__apps] 
    
    def getAllAppsProcessingNames(self): #Pending
        ''' return a list all apps procName '''
        
        if self.__appsEmpty(): 
            return None
        else:
            return [p.getProcessingName() for p in self.__apps] 

    def getName(self):
        ''' return family name '''
        return self.__name
    
    def getProcessingName(self):
        ''' return family processing name '''
        return self.__processingName

    def getLocation(self):
        ''' return family path '''
        return self.__location

    def getSize(self):
        ''' return the total number of apps in the family '''
        return len(self.__apps)

    #setters
    def setFeatureSelected(self, featureSelected):
        ''' set feature selected '''
        self.__featuresSelected = featureSelected

    #Private    
    def __checkPathsExistance(self, loc):
        ''' check existnace family path'''
        if os.path.exists(loc):
            return True
        else:
            raise ValueError('Family folder: not found at location: ', loc)
            return False
    def __appsEmpty(self):
        if self.__apps == []:
            print("No apps detected in ", self.__name)
            return True
        return False