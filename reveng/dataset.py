# python3 -m reveng.dataset
'''
reveng tool 
- make sure the following lib are existed:
.. etc
'''
from . import app, family
import os 
import subprocess
import multiprocessing 
import threading
import time 
import json

class DataSet(object):
    ''' dataset class '''
    #Public:
    def __init__(self):
        ''' init DataSet '''
        # dataset info
        self.__datasetFoldername = ''
        self.__datasetPath = ''
        # feature info
        self.__featureFilename = ''
        self.__featurePath = ''
        # collection info
        self.__collectionFoldername = ''
        self.__collectionPath = ''    
        # main OS
        self.__osBase = ''
        # output files
        self.__outputJson = ''
        self.__outputJsonPath = ''
        self.__outputFeature = ''
        self.__outputFeaturePath = ''
        self.__outputClasses = ''
        self.__outputClassesPath = ''
        # others members
        self.__featuresSelected = []
        self.__families = []
        # threading
        self.__work = multiprocessing.JoinableQueue()

        self.__checkOS()

    #ubunuto
    def load(self, threadCounts = 4,
            datasetPath = '/home/fahad/Desktop/Testing/vers/dataset',
            featurePath = '/home/fahad/Desktop/Testing/vers/feature.txt',
            collectionPath = '/home/fahad/Desktop/Testing/vers/collection',
            appExtension = '.apk', 
            appSeparator = '_',
            outputJsonPath = '/home/fahad/Desktop/Testing/vers/index.json',
            outputFeaturePath = '/home/fahad/Desktop/Testing/vers/featureCode.txt',
            outputClassesPath = '/home/fahad/Desktop/Testing/vers/classes.txt'): #done
        """
        #Darwin
        def load(self, 
            datasetPath = '/Users/fahad/Desktop/Testing/dataset',
            featurePath = '/Users/fahad/Desktop/Testing/feature.txt',
            collectionPath = '/Users/fahad/Desktop/Testing/collection',
            appExtension = '.apk', 
            appSeparator = '_',
            outputJsonPath = '/Users/fahad/Desktop/Testing/index.json',
            outputFeaturePath = '/Users/fahad/Desktop/Testing/featureCode.txt',
            outputClassesPath = '/Users/fahad/Desktop/Testing/classes.txt'): #done
        """
        ''' load the paths of all dataset '''
        try:
            # check osparse
            self.__prepareEnv(datasetPath,featurePath)
            
            # dataset
            self.__thCount = threadCounts
            self.__datasetPath = datasetPath
            self.__datasetFoldername = os.path.basename(datasetPath)
            # feature
            self.__featurePath = featurePath
            self.__featureFilename = os.path.basename(featurePath)
            # collection
            self.__collectionPath = collectionPath
            self.__collectionFoldername = os.path.basename(collectionPath)
            # Apps 
            app.App.extension = appExtension
            app.App.separator = appSeparator
            # outputs json
            self.__outputJsonPath = outputJsonPath
            self.__outputJson = os.path.basename(outputJsonPath)
            # output feature
            self.__outputFeaturePath = outputFeaturePath
            self.__outputFeature = os.path.basename(outputFeaturePath)
            # output classes
            self.__outputClassesPath = outputClassesPath
            self.__outputClasses = os.path.basename(outputClassesPath)
            
            # read feature from a file 
            self.__readSelectedFeaturePhrases()
            
            # load families using threads way2
            self.__loadDatasetContent()

        except TypeError as e:
            print('TypeError.args:', e.args)
            print('TypeError.message', e.message)
            print('Terminated due to some exceptions in Dataset')

    def generateJson(self):
        ''' generate json file with all info about dataset '''
        print(" -generating json path: {}..".format(self.__outputJsonPath))
        #----dataset---
        jData = dict()
        jData['size'] = self.getSize()
        jData['location'] = self.__datasetPath
        jData['processingLocation'] = ''
        jData['families'] = list()

        #----family----
        for f in self.getAllFamiliesObjs():
            fmly = dict()
            fmly['name'] = f.getName()
            fmly['processingName'] = f.getProcessingName()
            fmly['size'] = f.getSize()
            fmly['apps'] = list()

            #----app----
            for p in f.getAllAppsObjs():
                ap = dict()
                ap['name'] = p.getName()
                ap['processingName'] = p.getProcessingName()
                ap['package'] = p.getPackage()
                ap['feature'] = p.getPermissionPhrases()
                fmly['apps'].append(ap)
            jData['families'].append(fmly)
        data ={}
        data['dataset'] = jData
        # dump the jsonData dict in dataset.json file
        try:
            with open(self.__outputJsonPath, "w") as jOut:
                json.dump(data, jOut)
        except TypeError or NameError or EOFError as x:
            print("ERROR MSG:\n", x.args)
        
        print(" -file: {} is ready".format(self.__outputJson))

    #getters

    def getSelectedFeaturePhrases(self):
        ''' get the list of feature phrases '''
        return self.__featuresSelected

    def getAllFamiliesProcessingNames(self): 
        ''' list of processingNames of all families '''
        
        if self.__familiesEmpty(): 
            return None

        else:
            return ([fmly.getProcessingName() for fmly in self.__families])

    def getAllFamiliesNames(self): 
        ''' list of tuple(name, processingName) of the families '''

        if self.__familiesEmpty(): 
            return None
        else:
            return ([fmly.getName() for fmly in self.__families])

    def getAllFamiliesObjs(self):
        ''' return all families objects in the dataset'''
        if self.__familiesEmpty(): 
            return None
        return self.__families

    def getFamilyObj(self, name = '', procName = ''):
        ''' return a family object with name or procName'''

        if self.__familiesEmpty(): 
            return None
        
        result = [fmly for fmly in self.__families if fmly.getName() == name or fmly.getProcessingName() == procName]
        if len(result) > 1:
            print("conflict: More than one family with name:", name, "or processingName:", procName)
            return None
        elif len(result) < 1:
            print("No family with name:", name, "and processingName:", procName)            
            return None
        else:
            return result[0]
    
    def getFamilyName(self, procName = ''):
        ''' return a family name given procName'''
                
        if self.__familiesEmpty(): 
            return None
        result = ''
        for fmly in self.__families:
            if fmly.getProcessingName() == procName:
                result = fmly.getName()
                return result
        if result == '':
            print("No procName:{0} is detected in {1} family".format(procName, self.__name))

    def getFamilyProcessingName(self, name = ''):
        ''' return a family procName given name'''
        
        if self.__familiesEmpty(): 
            return None
        result = ''
        for fmly in self.__families:
            if fmly.getName() == name:
                result = fmly.getProcessingName()
                return result
        if result == '':
            print("No name:{0} is detected in {1} family".format(name, self.__name))

    def getSize(self):
        return len(self.__families)
    
    def getLargestFamilyObj(self): 
        ''' return the largest family in dataset '''
        if self.__familiesEmpty(): 
            return None
        else:
            fmly = self.__families[0]
            for f in self.__families:
                if f.getSize() > fmly.getSize():
                    fmly = f
            return fmly
    
    def getSmallestFamilyObj(self): 
        ''' return the smallest family in dataset '''
        if self.__familiesEmpty(): 
            return None
        else: 
            fmly = self.__families[0]
            for f in self.__families:
                if f.getSize() < fmly.getSize():
                    fmly = f
            return fmly
    
    #Private:
    def __prepareEnv(self, datasetPath, featurePath): 
        ''' check existance paths (dataset, feature) and clear prev output '''
        if self.__checkPathsExistance(datasetPath,featurePath):
            self.__cleanPreviousOutput()
        

    def __checkOS(self): 
        ''' check if Darwin or Lunix '''
        if not os.uname().sysname == 'Darwin' and not os.uname().sysname == 'Linux':
            raise OSError('System not supported')
        
    def __checkPathsExistance(self, datasetPath, featurePath): 
        ''' check existnace of all paths dataSet, featureFilename, etc '''
        if os.path.exists(datasetPath):
            return True
        else:
            raise ValueError('Dataset folder: not found at location: ', datasetPath)
            return False

        if os.path.exists(featurePath):
            #print('Feature file: found')
            return True
        else:
            raise ValueError('Feature file: not found at location: ', featurePath)
            return False

    def __cleanPreviousOutput(self): 
        ''' delete collection and output(classes, json, featureFolder)'''
        if os.path.exists(self.__collectionPath): 
            subprocess.check_output(['rm', '-r', self.__collectionPath], shell=False)
        
        if os.path.exists(self.__outputClassesPath): 
            subprocess.check_output(['rm', '-r', self.__outputClassesPath], shell=False)
        
        if os.path.exists(self.__outputJsonPath): 
            subprocess.check_output(['rm', '-r', self.__outputJsonPath], shell=False)
        
        if os.path.exists(self.__outputFeaturePath): 
            subprocess.check_output(['rm', '-r', self.__outputFeaturePath], shell=False)

    def __readSelectedFeaturePhrases(self): 
        ''' read main feature file '''
        with open(self.__featurePath) as ftFile:   
            self.__featuresSelected = [f[:-1] for f in ftFile.readlines()]  # -1 for \n at the end of a permission phrase

    def __getDirectories(self):
        ''' return the list of directory in the dataset path'''
        dirs = [dir for dir in os.listdir(self.__datasetPath) if not dir.startswith('.') and os.path.isdir(os.path.join(self.__datasetPath, dir))]
        return dirs
    
    def __loadDatasetContent(self):
        ''' loading dataset cont threading '''
        dirs = self.__getDirectories()
        self.__families = [None] * len(dirs)
        
        # start time
        print("-processing..")
        ndx = 0
        start = time.time()
        for f in dirs:
            path = os.path.join(self.__datasetPath, f)
            procName = str(ndx)
            # <fam_> <famProcName> 
            fobj = family.Family(path, procName) # create a family
            fobj.setFeatureSelected(self.__featuresSelected) # set feature
            self.__families[ndx] = fobj
            ndx = ndx + 1
        
        for i in range(self.getSize()):
            self.__work.put(i)
        qTotal = self.__work.qsize()

        for t in range(self.__thCount):
            t = threading.Thread(target=self.__run)
            t.daemon = True
            t.start()
        thCount = threading.active_count()
        
        self.__work.join()
        # end time
        print(" -total:", format(time.time() - start, '.2f'), " -threads:", thCount, " -worked on", qTotal, "tasks")
        
        # stop threads by putting None in queue
        for i in range(self.__thCount):
            self.__work.put(None)
        
    def __job(self, ndx):
        ''' thread job'''
        #load Apps in familes
        self.__families[ndx].loadFamilyContent()
        self.__families[ndx].parse()
        self.__families[ndx].extractFeature()

    def __run(self):
        ''' thread target func '''
        while True:
            ndx = self.__work.get()
            # stop the thread
            if ndx == None:
                break
            self.__job(ndx)
            self.__work.task_done()
            
    def __familiesEmpty(self):
        if self.__families == []:
            print("No families detected in ", self.__name)
            return True
        return False